$(document).ready(function() {
    selectProject();
});

function selectProject(){
    var post = '';

    var request = $.ajax({
        url: "./ajax_management?rand="+escape(Math.random()),
        type: "POST", data: post, dataType: "html"
    });

    request.done(function(msg) {
        if(msg){
            $('#Project').html(msg);
        }
    });request.fail(function(jqXHR, textStatus) {/*alert( "z-Request failed: " + textStatus );*/});
}

function activeProject(i){
    var post = '';
    post = post + '&i='+i;

    var request = $.ajax({
        url: "./ajax_management_active?rand="+escape(Math.random()),
        type: "POST", data: post, dataType: "html"
    });

    request.done(function(msg) {
        if(msg){
            alert(msg);
        }
    });request.fail(function(jqXHR, textStatus) {/*alert( "z-Request failed: " + textStatus );*/});
}

(function viewActiveProject() {
  $.ajax({
    url: "./ajax_view_active?rand="+escape(Math.random()),
    type: "POST",
    dataType: "html",
    success: function(data) {
        alert(data);
    },
    complete: function() {
      // Schedule the next request when the current one's complete
      setTimeout(viewActiveProject, 3000);
    }
  });
})();

