#!/usr/bin/env python
#-*- coding:utf-8 -*-import json

import webbrowser
import threading
from Bottle.bottle import route, run, template, get, post, request, static_file, response, app
from Mylib import config as CONF, html_escape
from Mylib import crypt as CRY
from beaker.middleware import SessionMiddleware
import beaker.middleware
session_opts = {
    'session.type': 'file',
    'session.data_dir': './session/',
    'session.auto': True,
}
app = beaker.middleware.SessionMiddleware(app(), session_opts)
#from Mylib.interrogami import readLocalita
# coockie in funzioni
#response.set_cookie("My_NAME", "Is Leosx", path='/', httponly=True) Crea cookie
#request.get_cookie("My_NAME") legge cookie
#response.set_cookie('My_NAME', "", max_age=-1,) cancella cookie



##### FILE STATICI ##################################################
@get('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root=CONF.CONFIG['STATIC'])

##### ========= ##################################################

@route('/<urls:re:[a-z0-9\/-]+>')
@route('/<urls:re:[a-z0-9\/-]+>', method='GET')
@route('/<urls:re:[a-z0-9\/-]+>', method='POST')
def pages(urls):
    from new_service import view

    if urls == False:
        return html_escape.unescape(view.homePage(urls))
    if urls == "create-project" or urls == "create-project/":
        request.session = request.environ['beaker.session']
        print request.session['something']
        return html_escape.unescape(view.create(urls, request))
    if urls == "management-project" or urls == "management-project/":
        return html_escape.unescape(view.management(urls, request))

@get('/ajax_<urls:re:[a-z0-9\/-_]+>')
@get('/ajax_<urls:re:[a-z0-9\/-_]+>', method='POST')
def ajax(urls):
    from new_service import ajax
    if urls == "management" or urls == "management/":
        return ajax.ajax_management(request)
    '''
    if urls == "management_active" or urls == "management_active/":
        return ajax.ajax_management_active(request)
    '''
    if urls == "view_active" or urls == "view_active/":
        return ajax.ajax_view_active(request)

### DEFAULT #################################################
@route('/')
def index(urls=False): 
    # sessioni 
    #request.session = request.environ['beaker.session']
    #request.session['something'] = "HELLO WORLD"
    #print request.session['something']
    return pages(urls)
### AVVIA WEBROWSER ############################################## forse!!!!
def webBrowsers():
    # apre una nuova tab del browser predefinito passandogli l'indirizzo di destinazione
    return webbrowser.open_new_tab(CONF.CONFIG['URL'])
    #return webbrowser.open_new(CONF.CONFIG['URL']+'new-service')

def runner():
    #avvia la richiesta di funzione (webBrowser) dopo n secondi (in questo caso 1)
    return threading.Timer(1, webBrowsers).start()
runner()

run(host=CONF.CONFIG['HOST'], port=CONF.CONFIG['PORT'], debug=True, app=app)


