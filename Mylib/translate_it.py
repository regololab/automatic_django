#-*- coding:utf-8 -*-
TRANSLATE = {
    "DESCRIPTION_DAS":"Django Easy Starter - Home",
    "ESPANDI_SCHERMO":"Espandi / Contrai Schermo",
    "BENVENUTI":"Benvenuti in D.E.S. - Django Easy Starter",
    # HOME
    "CREAZIONE_LAUNCHER":"Creazione Launchers",
    "CREAZIONE_LAUNCHER_DESC":"Permette di creare nuovi Launchers per progetti Django",
    "VAI":"Vai",
    "ATTIVA_PROGETTI":"Attiva/Disattiva Progetti",
    "ATTIVA_PROGETTI_DESC":"Questa procedura permette di abilitare/disabilitare un progetto Django",
    # CREATE
    "PROGETTO":"Nome Progetto:",
    "VIRTUALENV_ADDRESS":"Indirizzo Virtualenv:",
    "VIRTUALENV_ADDRESS_DESC":"es. /home/tua/dir/virtual/",
    "PROGETTO_ADDRESS":"Indirizzo Progetto",
    "PROGETTO_ADDRESS_DESC":"es. /home/tua/dir/project/",
    "HOST_PROGETTO":"Host Progetto",
    "HOST_PROGETTO_DESC":"es. localhost:8000",
    "PROG_ATTIVI":"Progetti Presenti",
    "GESTIONE":"Azioni",
    "MODIFICA_DATO":"Modifica Dato",
    "CANCELLA_DATO":"Cancella Dato",
    "MS_ERR_01":"Attenzione: tutti i campi sono obbligatori",
    "MS_ERR_02":"Attenzione: questo progetto è già presente nel DB",
    "MS_OK_01":"Operazione Eseguita Correttamente",
    "MS_OK_02":"Modifica Eseguita Correttamente",
    "MS_DELETE_01":"Sicuri di voler cancellare quasto dato?",
    # MANAGEMENT
    "GEST_PROG":"Gestione Progetti",
}


