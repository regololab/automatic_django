#!/usr/bin/env python
#-*- coding:utf-8 -*-
import os

def ActiveProcess(prox):
    pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]

    process = []
    for pid in pids:
        try:
            proc = str(open(os.path.join('/proc', pid, 'cmdline'), 'rb').read())
            if proc.find(prox) > -1:
                process.append({
                    "id":pid,
                    "Process":proc,
                })
        except IOError: # proc has already terminated
            continue
    return process