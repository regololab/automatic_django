#-*- coding:utf-8 -*-
import sqlite3 as lite
from config import CONFIG

SQlite = lite.connect(CONFIG['ADDRESS']+'db/das.sqlite')
CURlite = SQlite.cursor()

def connectionClose():
    SQlite.close()

def escapeSql(frase):
    return frase.replace("'", "''")
