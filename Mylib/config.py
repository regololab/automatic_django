#-*- coding:utf-8 -*-

import os
import time

CONFIG = {
    # PROJECT
    "STATIC_VAR": {
        "PROJECT":"D.E.S. b.0.1",
        "AUTHOR": "Leo Lo Tito",
        "TITLE": "DES - Django Easy Starter",
        "DESCRIPTION": "DES - Django Easy Starter....",
        #Date
        "DATE":time.strftime('%Y/%m/%d'),
        "DATATIME":time.strftime('%Y/%m/%d %H:%M%S'),
        "YARS":time.strftime('%Y'),
        "TIME":time.strftime('%H:%M%S'),
        "TERMINAL":"gnome-terminal",
    },
    # SECURE CRYPT
    "KEYS": "A!-/&Hgj87.GIpo*",
    "LSALT":"ChangeMe!",
    "RSALT":"ChangeMe!",
    #"RSI": request.headers.values()[2] + request.headers.values()[3] + request.headers.values()[5] + request.headers.values()[8],
    # ROOT
    "ADDRESS":os.path.dirname(os.path.abspath(__file__)).replace("/Mylib","") + '/',
    "STATIC":os.path.dirname(os.path.abspath(__file__)).replace("/Mylib","").replace("/create-project","") + '/static/',
    "HOST":"localhost",
    "PORT":"8080",
    "URL":"http://localhost:8080/",
    #DB
    "SQL_USER":"postgres",
    "SQL_PASS":"qazxsw",
    "SQL_NAME":"middleware",
    "SQL_HOST":"192.168.1.100",
    "SQL_PORT":"5432",

}