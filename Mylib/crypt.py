import random
from Crypto.Cipher import AES
from Mylib import config as CONF

def _pad(s):
    return s + b"\0" * (AES.block_size - len(s) % AES.block_size)

def _keys():
    return CONF.CONFIG['KEYS']

def encrypt(message, key_size=256):
    message = _pad(message)
    iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
    cipher = AES.new(_keys(), AES.MODE_CBC, iv)
    return iv + cipher.encrypt(message)

def decrypt(ciphertext):
    iv = ciphertext[:AES.block_size]
    cipher = AES.new(_keys(), AES.MODE_CBC, iv)
    plaintext = cipher.decrypt(ciphertext[AES.block_size:])
    return plaintext.rstrip(b"\0")

def encrypt_file(file_name):
    with open(file_name, 'rb') as fo:
        plaintext = fo.read()
    enc = encrypt(plaintext, _keys())
    with open(file_name + ".enc", 'wb') as fo:
        fo.write(enc)

def decrypt_file(file_name):
    with open(file_name, 'rb') as fo:
        ciphertext = fo.read()
    dec = decrypt(ciphertext, _keys())
    with open(file_name[:-4], 'wb') as fo:
        fo.write(dec)