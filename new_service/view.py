#-*- coding:utf-8 -*-
import os
import stat
import subprocess
from Bottle.bottle import template
from Mylib import html_escape, config as CONF, translate_it as TRANS, process as PROC
from Mylib.db import (SQlite, CURlite)
from Mylib.db import connectionClose, escapeSql


def homePage(Url):
    ## GESTION TEMPLATE ##########################################################
    page = template(CONF.CONFIG['ADDRESS']+'new_service/template/home.tpl', {
        "TRANSLATE": TRANS.TRANSLATE,
        "URL": CONF.CONFIG['URL'],
    })

    # INVIO ALLA PAGINA PRINCIPALE ################################################
    output = template(CONF.CONFIG['ADDRESS']+'templates/template.tpl', {
        "STATIC_VAR": CONF.CONFIG['STATIC_VAR'],
        "TRANSLATE": TRANS.TRANSLATE,
        "PAGE":"__home__1",
        "URL": CONF.CONFIG['URL'],
        "ADDRESS": CONF.CONFIG['ADDRESS'],
        "CONTENT_PP":"",
        ##################################
        "INCLUDE_CONTENITORE": html_escape.unescape(page),
    })
    return output

def create(Url, request):
    nome = escapeSql(str(request.POST.get('nome'))) if request.POST.get('nome') else ''
    nome = nome.strip()
    virtual = escapeSql(str(request.POST.get('virtual'))) if request.POST.get('virtual') else ''
    virtual = virtual.strip()
    project = escapeSql(str(request.POST.get('progetto'))) if request.POST.get('progetto') else ''
    project = project.strip()
    host = escapeSql(str(request.POST.get('host'))) if request.POST.get('host') else ''
    host = host.strip()

    ms_err = ""
    ms_ok = ""

    if request.POST.get('rec') and not request.POST.get('update'):
        if len(nome) > 1 and len(virtual) > 1 and len(project) > 1 and len(host) > 1:
            sql = "SELECT id FROM project WHERE name = '{0}' LIMIT 0,1".format(nome)
            CURlite.execute(sql)
            exists = CURlite.fetchone()
            if exists is None:
                sql = "INSERT INTO project(name, virtualenv, address, host, state) VALUES('{0}','{1}','{2}','{3}','{4}')".format(nome, virtual, project, host, '0')
                CURlite.execute(sql)
                SQlite.commit()
                last_id = CURlite.lastrowid

                gen_file = "#!/bin/sh\n\n"\
                        "#{nome}\n\n{virtual}python {project}manage.py runserver {host} > {static}log/{nome}.log 2>&1\n"\
                        " ".format(nome=nome, virtual=virtual, project=project, host=host, static=CONF.CONFIG['STATIC'])
                out_file = open("{0}create/__{1}.sh".format(CONF.CONFIG['STATIC'], last_id), "w")
                out_file.write(gen_file)
                out_file.close()
                st = os.stat("{0}create/__{1}.sh".format(CONF.CONFIG['STATIC'], last_id))
                os.chmod("{0}create/__{1}.sh".format(CONF.CONFIG['STATIC'], last_id), st.st_mode | stat.S_IEXEC)

                ms_ok = TRANS.TRANSLATE['MS_OK_01']
            else:
                ms_err = TRANS.TRANSLATE['MS_ERR_02']
        else:
            ms_err = TRANS.TRANSLATE['MS_ERR_01']
    elif  request.POST.get('rec') and request.POST.get('update'):
        if len(nome) > 1 and len(virtual) > 1 and len(project) > 1 and len(host) > 1:
            pk = escapeSql(str(request.POST.get('update'))) if request.POST.get('update') else ''
            pk = pk.strip()
            sql = "UPDATE project SET virtualenv='{virtual}', address='{project}', host='{host}' WHERE id='{pk}'".format(pk=pk, virtual=virtual, project=project, host=host)
            CURlite.execute(sql)
            SQlite.commit()

            gen_file = "#!/bin/sh\n\n"\
                        "#{nome}\n\n{virtual}python {project}manage.py runserver {host} > {static}log/{nome}.log 2>&1\n"\
                        " ".format(nome=nome, virtual=virtual, project=project, host=host, static=CONF.CONFIG['STATIC'])
            out_file = open("{0}create/__{1}.sh".format(CONF.CONFIG['STATIC'], pk), "w")
            out_file.write(gen_file)
            out_file.close()
            ms_ok = TRANS.TRANSLATE['MS_OK_02']

    if request.GET.get('del'):
        pk = request.GET.get('i')
        sql = "DELETE FROM project WHERE id='{pk}'".format(pk=pk,)
        CURlite.execute(sql)
        SQlite.commit()
        try:
            os.unlink("{0}create/__{1}.sh".format(CONF.CONFIG['STATIC'], pk))
        except:
            pass


    sql = "SELECT * FROM project ORDER BY name ASC"
    CURlite.execute(sql)
    result = CURlite.fetchall()

    ## GESTION TEMPLATE ##########################################################
    page = template(CONF.CONFIG['ADDRESS']+'new_service/template/create.tpl', {
        "TRANSLATE": TRANS.TRANSLATE,
        "URL": CONF.CONFIG['URL'],
        "MESSAGE_ERR":ms_err,
        "MESSAGE_OK":ms_ok,
        "PROJECT_RESULT":result,
    })

    # INVIO ALLA PAGINA PRINCIPALE ################################################
    output = template(CONF.CONFIG['ADDRESS']+'templates/template.tpl', {
        "STATIC_VAR": CONF.CONFIG['STATIC_VAR'],
        "TRANSLATE": TRANS.TRANSLATE,
        "PAGE":"__home__2",
        "URL": CONF.CONFIG['URL'],
        "ADDRESS": CONF.CONFIG['ADDRESS'],
        "CONTENT_PP":"",
        ##################################
        "INCLUDE_CONTENITORE": html_escape.unescape(page),
    })
    return output

def management(Url, request):
    ms_err = ""
    ms_ok = ""
    pk = request.GET.get('i')

    if pk > 0:
        sql = "SELECT * FROM project WHERE id='{pk}' LIMIT 1".format(pk=pk)
        CURlite.execute(sql)
        R = CURlite.fetchone()
        if R is not None:
            attivo = PROC.ActiveProcess(R[2])
            path = CONF.CONFIG['STATIC'] + "create/__{0}.sh".format(str(R[0]))
            cmd = "{terminal} --tab -e {sh}".format(terminal= CONF.CONFIG['STATIC_VAR']['TERMINAL'], sh=path)
            subprocess.call(cmd, shell=True)


    ## GESTION TEMPLATE ##########################################################
    page = template(CONF.CONFIG['ADDRESS']+'new_service/template/management.tpl', {
        "TRANSLATE": TRANS.TRANSLATE,
        "URL": CONF.CONFIG['URL'],
        "MESSAGE_ERR":ms_err,
        "MESSAGE_OK":ms_ok,
    })

    # INVIO ALLA PAGINA PRINCIPALE ################################################
    output = template(CONF.CONFIG['ADDRESS']+'templates/template.tpl', {
        "STATIC_VAR": CONF.CONFIG['STATIC_VAR'],
        "TRANSLATE": TRANS.TRANSLATE,
        "PAGE":"__home__3",
        "URL": CONF.CONFIG['URL'],
        "ADDRESS": CONF.CONFIG['ADDRESS'],
        "CONTENT_PP":"<script type=\"text/javascript\" src=\""+CONF.CONFIG['URL']+"static/js/managment.js\"></script>",
        ##################################
        "INCLUDE_CONTENITORE": html_escape.unescape(page),
    })
    return output

