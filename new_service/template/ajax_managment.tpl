<div class="list-group">
    <span href="#" class="list-group-item active" style="height:45px;">
        <span style="float:left;width:74%;font-weight:bold;"> Progetto</span>
        <span style="float:left;font-weight:bold;width:10%;margin-left:0.5%;margin-right:0.5%;text-align:center;"> Stato</span>
        <span style="float:left;font-weight:bold;width:10%;margin-left:0.5%;margin-right:0.5%;"> Azioni</span>
    </span>
    % for R in result:
        <span href="#" class="list-group-item" style="height:45px;">
            <span style="float:left;width:74%;"> <a href="http://{{R[4]}}" target="_blank"><i class="fa fa-share-square-o"></i> {{R[1]}} </a> </span>
            <span style="float:left;width:10%;margin-left:0.5%;margin-right:0.5%;text-align:center;">
                <a href="{{URL}}management-project?i={{R[0]}}">
                <i class="fa fa-lightbulb-o fa-2x curp text-red" id="active_{{R[0]}}" title="Attiva/Disattiva Progetto"></i>
                </a>
            </span>
            <span style="float:left;width:10%;margin-left:0.5%;margin-right:0.5%;">
                <i class="fa fa-file-text fa-2x text-blue curp" title="Visualizza Log"></i>
                &nbsp;&nbsp;&nbsp;
                <i class="fa fa-trash fa-2x text-red curp" title="Svuota Log"></i>
            </span>
        </span>
        <span class="" id="log_{{R[0]}}"></span>
    % end
</div>