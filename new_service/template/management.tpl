<div class="col-lg-12">
    <br/>
    <ol class="breadcrumb">
        <li><a href="../">{{TRANSLATE['DESCRIPTION_DAS']}}</a>
        </li>
        <li class="active">{{TRANSLATE['ATTIVA_PROGETTI']}}</li>
    </ol>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><b>{{TRANSLATE['GEST_PROG']}}</b></h3>
            </div>
            <div class="panel-body">
                <span id="Project"></span>
            </div>
        </div>
    </div>
</div>
