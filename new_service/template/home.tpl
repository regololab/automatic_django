<div class="col-lg-12 ">
    <h1 class="page-header">
        <b>{{TRANSLATE['BENVENUTI']}}</b>
    </h1>
</div>

<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading bgcolor-blue2A text-white">
            <h4><i class="fa fa-heartbeat"></i> {{TRANSLATE['CREAZIONE_LAUNCHER']}}</h4>
        </div>
        <div class="panel-body">
            <p>{{TRANSLATE['CREAZIONE_LAUNCHER_DESC']}}</p>
            <a href="create-project" class="btn btn-primary">{{TRANSLATE['VAI']}} <i class="fa fa-chevron-right"></i></a>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading bgcolor-blue2A text-white">
            <h4><i class="fa fa-tasks"></i> {{TRANSLATE['ATTIVA_PROGETTI']}}</h4>
        </div>
        <div class="panel-body">
            <p>{{TRANSLATE['ATTIVA_PROGETTI_DESC']}} </p>
            <a href="management-project" class="btn btn-primary">{{TRANSLATE['VAI']}} <i class="fa fa-chevron-right"></i></a>
        </div>
    </div>
</div>