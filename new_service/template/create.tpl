<div class="col-lg-12">
    <br/>
    <ol class="breadcrumb">
        <li><a href="../">{{TRANSLATE['DESCRIPTION_DAS']}}</a>
        </li>
        <li class="active">{{TRANSLATE['CREAZIONE_LAUNCHER']}}</li>
    </ol>
</div>

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading bgcolor-blue2A text-white">
            <h4><i class="fa fa-heartbeat"></i> {{TRANSLATE['CREAZIONE_LAUNCHER']}}</h4>
        </div>
        <div class="panel-body">
            %if len(MESSAGE_ERR) > 0:
                <div class="alert alert-danger" role="alert">
                    <span class="fa fa-thumbs-down" aria-hidden="true"></span>
                    {{MESSAGE_ERR}}
                </div>
            %end
            %if len(MESSAGE_OK) > 0:
                <div class="alert alert-success" role="success">
                    <span class="fa fa-thumbs-up" aria-hidden="true"></span>
                    {{MESSAGE_OK}}
                </div>
            %end
            <form action="{{URL}}create-project" method="post" name="create_project">
                  <div class="form-group">
                    <label for="exampleInputEmail1">{{TRANSLATE['PROGETTO']}}</label>
                    <input type="text" class="form-control" name="nome" id="nome" placeholder="{{TRANSLATE['PROGETTO']}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">{{TRANSLATE['VIRTUALENV_ADDRESS']}}</label>
                    <input type="text" class="form-control" name="virtual" id="virtual" placeholder="{{TRANSLATE['VIRTUALENV_ADDRESS_DESC']}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">{{TRANSLATE['PROGETTO_ADDRESS']}}</label>
                    <input type="text" class="form-control" name="progetto" id="progetto" placeholder="{{TRANSLATE['PROGETTO_ADDRESS_DESC']}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">{{TRANSLATE['HOST_PROGETTO']}}</label>
                    <input type="text" class="form-control" name="host" id="host" placeholder="{{TRANSLATE['HOST_PROGETTO_DESC']}}">
                  </div>
                    <input type="hidden" name="rec" value="1" id="rec" />
                    <input type="hidden" name="update" value="" id="update" />
                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> {{TRANSLATE['VAI']}}</button>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button type="reset" class="btn btn-warning" onclick="$('#update').val('');$('#nome').removeAttr('readonly');"><i class="fa fa-refresh"></i> Reset</button>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><b>{{TRANSLATE['PROG_ATTIVI']}}</b></h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" id="example2">
                  <thead>
                    <tr>
                      <th>{{TRANSLATE['PROGETTO']}}</th>
                      <th>{{TRANSLATE['VIRTUALENV_ADDRESS']}}</th>
                      <th>{{TRANSLATE['PROGETTO_ADDRESS']}}</th>
                      <th>{{TRANSLATE['HOST_PROGETTO']}}</th>
                      <th>{{TRANSLATE['GESTIONE']}}</th>
                    </tr>
                  </thead>
                  <tbody>
                    % for R in PROJECT_RESULT:
                        <tr>
                            <td>{{R[1]}}</td>
                            <td>{{R[2]}}</td>
                            <td>{{R[3]}}</td>
                            <td>{{R[4]}}</td>
                            <td>
                                <button type="button" class="btn btn-primary btn-sm" title="{{TRANSLATE['MODIFICA_DATO']}}" onclick="modifyPj('{{R[0]}}', '{{R[1]}}','{{R[2]}}', '{{R[3]}}', '{{R[4]}}');"><i class="fa fa-edit curp" ></i></button>
                                <button type="button" class="btn btn-danger btn-sm" title="{{TRANSLATE['CANCELLA_DATO']}}" onclick="deletePj('{{R[0]}}');"><i class="fa fa-trash curp" ></i></button>
                            </td>
                        </tr>
                    % end
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
