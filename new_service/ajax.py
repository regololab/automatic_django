#-*- coding:utf-8 -*-
import os
import stat
import subprocess
import json
from Bottle.bottle import template
from Mylib import html_escape, config as CONF, translate_it as TRANS, process as PROC
from Mylib.db import (SQlite, CURlite)
from Mylib.db import connectionClose, escapeSql

def ajax_management(request):
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        sql = "SELECT * FROM project ORDER BY name ASC"
        CURlite.execute(sql)
        result = CURlite.fetchall()

        output = template(CONF.CONFIG['ADDRESS']+'new_service/template/ajax_managment.tpl', {
            "STATIC_VAR": CONF.CONFIG['STATIC_VAR'],
            "TRANSLATE": TRANS.TRANSLATE,
            "ADDRESS": CONF.CONFIG['ADDRESS'],
            "URL": CONF.CONFIG['URL'],
            ##################################
            "result":result,
        })
        return output

'''
def ajax_management_active(request):
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        pk = request.POST.get('i')

        sql = "SELECT * FROM project WHERE id='{pk}' LIMIT 1".format(pk=pk)
        CURlite.execute(sql)
        R = CURlite.fetchone()
        if R is not None:
            attivo = PROC.ActiveProcess(R[2])
            subprocess.call(CONF.CONFIG['STATIC'] + "create/__{0}.sh".format(str(R[0])))
            return pk
        else:
            return -1
'''

def ajax_view_active(request):
    active = []
    process = []
    sql = "SELECT id, virtualenv FROM project "
    CURlite.execute(sql)
    for R in CURlite.fetchall():
        process = PROC.ActiveProcess(R[1])
        if len(process) > 1:
            active.append({
                "pk":R[0],
            })
    return json.dumps(active)
