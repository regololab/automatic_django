
<script type="text/javascript" src="/static/js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="/static/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/static/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/static/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/static/js/screenfull.js"></script>

<!-- page-specific script -->
<script type="text/javascript">
    $(function() {
        $('#iconsFullPage').html('<i class="fa fa-expand fa-stack-2x" style="color:#ffffff;"></i>');
        if(screenfull){$('#supported').text('Yes');} else {$('#supported').text('No');}
        if ( !screenfull ) {return false;}

        $('#FullScreen').click(function() {
            screenfull.toggle();
            if(screenfull.isFullscreen){
                $('#fullBack').css({"color":"#0E58A0"});
                $('#iconsFullPage').html('<i class="fa fa-expand fa-stack-2x" style="color:#ffffff;"></i>');
            } else {
                $('#fullBack').css({"color":"#FF5E00"});
                $('#iconsFullPage').html('<i class="fa fa-compress fa-stack-2x" style="color:#ffffff;"></i>');
            }
        });


        // Trigger the onchange() to set the initial values
        screenfull.onchange();
    });
</script>
<script type="text/javascript">
    $(function() {
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true
        });
    });
    function modifyPj(i, n, a1, a2, h){
        $('#update').val(i);
        $('#nome').val(n);
        $('#nome').prop('readonly',true);
        $('#virtual').val(a1);
        $('#progetto').val(a2);
        $('#host').val(h);
    }
    function deletePj(i){
        var r = confirm("{{TRANSLATE['MS_DELETE_01']}}");
        if (r == true) {
            window.location.href = '{{URL}}create-project/?i='+i+'&del=1';
        } else {
            false;
        }
    }
</script>