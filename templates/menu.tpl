<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">{{STATIC_VAR['PROJECT']}}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            %if PAGE=="__home__1":
                <a class="navbar-brand selected text-black2 bgcolor-white" href="./" title="{{TRANSLATE['DESCRIPTION_DAS']}}"><i class="fa fa-desktop fa-lg"></i></a>
            %else:
                <a class="navbar-brand text-black2 bgcolor-white" href="./"  title="{{TRANSLATE['DESCRIPTION_DAS']}}"><i class="fa fa-desktop"></i></a>
            %end
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <!--<span class="navbar-brand navbar-right noview" title="{{TRANSLATE['ESPANDI_SCHERMO']}}">
                <span class="fa-stack" id="FullScreen" style="margin-top:-10px;cursor:pointer;">
                    <i class="fa fa-square-o fa-stack-2x" id="fullBack" style="color:#0E58A0;"></i>
                    <span id="iconsFullPage"></span>
                </span>
            </span>-->
            <ul class="nav navbar-nav navbar-left" >
                <li>
                    %if PAGE=="__home__2":
                        <a href="/create-project" class="selected" title="Crea un nuovo progetto">
                            <i class="fa fa-heartbeat"></i> Creazione
                        </a>
                    %else:
                        <a href="/create-project" title="Crea un nuovo progetto">
                            <i class="fa fa-heartbeat"></i> Creazione
                        </a>
                    %end
                </li>
                <li>
                    %if PAGE=="__home__3":
                        <a href="/management-project" class="selected" title="Attiva/Disattiva progetto in VirtualEnv">
                            <i class="fa fa-tasks"></i> Gestione
                        </a>
                    %else:
                        <a href="/management-project" title="Attiva/Disattiva progetto in VirtualEnv">
                            <i class="fa fa-tasks"></i> Gestione
                        </a>
                    %end
                </li>
                <li>
                    %if PAGE=="__home__1":
                        <a href="/info" title="Informazioni sul funzionamento">
                            <i class="fa fa-info-circle"></i> Info
                        </a>
                    %else:
                        <a href="/info" title="Informazioni sul funzionamento">
                            <i class="fa fa-info-circle"></i> Info
                        </a>
                    %end
                </li>
            </ul>
        </div>
    </div>
</nav>